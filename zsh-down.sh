#!/bin/bash

# This script is actually meant to be fetched by curl and piped to bash therefore, this script has been wrapped in a main function and executes it at the bottom
# This is done to prevent code running before it is meant to or in an incomplete manner 

main() {
    curl -s -o /tmp/zsh.sh -L https://codeberg.org/firebadnofire/zsh-deploy/raw/branch/main/zsh.sh
    curl -s -o /tmp/fastfetch.sh -L https://codeberg.org/firebadnofire/zsh-deploy/raw/branch/main/fastfetch.sh
    clear
    chmod +x /tmp/zsh.sh
    chmod +x /tmp/fastfetch.sh
    echo "run /tmp/zsh.sh"
}

# Execute the main function
main
