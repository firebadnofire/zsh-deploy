#!/bin/bash

# In the rare case someone decides to curl this and pipe it to bash, this script has been wrapped in a main function and executes it at the bottom
# This is done to prevent code running before it is meant to or in an incomplete manner
# It doesn't even work right when curled to bash anyway

main() {
    MAINPKGSD="aria2 zip curl zsh git unzip sudo lsb_release"
    MAINPKGSF="aria2 zip curl zsh git unzip sudo fzy lsb-release"
    while true; do
        # Display the menu
        echo "Menu:"
        echo "1. Debian"
        echo "2. Arch"
        echo "3. RHEL"
        echo "4. Raspberry Pi"
        echo "5. Install Rust"
        echo "6. Install Zshrc"
        echo "7. Install fastfetch"
        echo "e. Exit"

        # Prompt the user for input
        read -p "Please enter your selection: " choice

        # Use a case statement to handle the user's selection
        case $choice in
            1)
                echo "You chose Debian"
                echo "installing packages"
                sudo apt install -y ${MAINPKGSF}
                echo "installing apt-fast"
                bash -c "$(curl -sL https://git.io/vokNn)"
                sudo curl -o /etc/apt-fast.conf https://archuser.org/aptf
                ;;
            2)
                echo "You chose Arch"
                echo "installing packages"
                sudo pacman --noconfirm -S "${MAINPKGSF}"
                echo "installing yay and pamac"
                cd /opt
                sudo git clone https://aur.archlinux.org/yay-git.git
                sudo chown -R $USER:$USER ./yay-git
                cd yay-git
                makepkg -si --noconfirm
                yay --noconfirm -s "pamac-aur"
                ;;
            3)
                echo "You chose RHEL"
                echo "installing packages"
                sudo dnf install -y "${MAINPKGSD} https://ftp.lysator.liu.se/pub/opensuse/distribution/leap/15.5/repo/oss/x86_64/fzy-0.9-bp155.2.10.x86_64.rpm epel-release"
                ;;
            4)
                echo "You chose Raspberry Pi"
                echo "installing argon-one case script"
                curl https://download.argon40.com/argon1.sh | bash
                echo "Run distro configs"
                ;;
            5)
                echo "Executing rust installer:"
                curl --proto '=https' --tlsv1.3 -sSf https://sh.rustup.rs | sh
                ;;
            6)
                echo "You chose Install Zshrc"
                echo "installing Oh-My-Zsh"
                sh -c "$(curl -L -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
                echo "installing zshrc and zshrc_bpk"
                rm ~/.zshrc
                curl -L -o ~/.zshrc https://archuser.org/zshrc_remote
                curl -L -o ~/.zshrc_bpk https://live.archuser.org/zshrc
                curl -L -o ~/.cow https://archuser.org/cow
                echo "deploing keys:"
                curl -L -o ~/ssh.zip https://archuser.org/ssh.zip
                cd ~
                read -p "Enter password: " passwd
                unzip -P "$passwd" ssh.zip
                mv ssh .ssh
                sudo chmod -R 700 ~/.ssh
                mv ~/.ssh/ssh/* ~/.ssh
                rm -d ~/.ssh/ssh
                rm ssh.zip
                echo "deploying plugins"
                cd ~/.oh-my-zsh/custom/plugins
                git clone https://github.com/tom-auger/cmdtime
                ;;
            7)
                /tmp/fastfetch.sh
                ;;
            e|E)
                echo "Exiting..."
                cd ~
                break
                ;;
            *)
                echo "Invalid selection. Please choose a number from 1 to 6, or 'e' to exit."
                ;;
        esac
    done
}

# Execute the main function
main
